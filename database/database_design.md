# 数据库设计

#### 1. attachment

-   附件数据表

-   字段（加粗为主键）：

    1. **attachment_id**

       附件id

    2. attachment_path

       附件路径

    3. attachment_desc

       附件文件名

#### 2. image

- 图像数据表

- 字段：

  1. **image_id**

     图像id

  2. Image_url

     图像路径

  3. image_desc

     图像文件名

#### 3. post

- 消息／通知公告

- 字段：

  1. **post_id**

     公告id

  2. post_text

     公告内容

  3. post_purchase

     与公告联通的采购项目

  4. post_type

     公告类型

  5. post_level_id

     公告层级id

  6. post_publish_time

     公告发布时间

  7. post_image_id

     公告图像id

  8. post_deleted

     公告是否已删除

  9. post_topic_id

     公告专题id

#### 4. post_attachment

- 公告——附件

- 字段：

  1. **post_attachment_id**

     公告——附件id

  2. post_id

     公告id

  3. attachment_id

     附件id

#### 5. post_level
- 公告——级别

- 字段：

  1. **post_level_id**

     公告——级别id

  2. post_name

     级别名称

  3. post_hasparent

     此级别是否有父级别

#### 6. post_topic
- 公告——话题

- 字段：

  1. **post_topic_id**

     公告——话题id

  2. post_topic_name

     话题名称

#### 7. supplier
- 供应商

- 字段：

  1. **supplier_id**

     供应商——id

  2. supplier_name

     供应商名称

  3. supplier_company_name

     供应商公司名称

  4. supplier_birthdate

     成立日期（？）

  5. supplier_type_id

     供应商类型id

  6. supplier_register_num

     供应商注册号

  7. supplier_register_capital

     供应商注册资本

  8. supplier_org_num

     机构代码（？？？）

  9. supplier_tax_num

     供应商税务登记号

  10. supplier_contact

     供应商联系人

  11. supplier_contact_tel

     供应商联系人电话

  12. supplier_email

     供应商邮件

  13. supplier_address

     供应商地址

  14. supplier_qq

     供应商qq号

  15. supplier_sign_id

     供应商登录id

  16. supplier_license_image_id

     供应商营业执照图片id

  17. supplier_scope

     供应商营业范围

  18. supplier_law_rep

     供应商法人代表

  19. supplier_isbanned

     供应商是否在黑名单

  20. supplier_idpassed

     审核是否通过

注：

	·供应商名称与公司名称是否重复？
	·birthdate?
	·机构代码？
	·营业日期
	·黑名单时间？
#### 8. supplier_sign
- 供应商登录

- 字段：

  1. **supplier_sign_id**

     供应商——id

  2. supplier_auth_name

     供应商授权人

  3. supplier_deposit

     保证金


#### 9. supplier_type
- 供应商类型

- 字段：

  1. **supplier_type_id**

     供应商类型——id

  2. supplier_type_name

     供应商类型名称




