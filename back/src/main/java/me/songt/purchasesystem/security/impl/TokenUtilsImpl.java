package me.songt.purchasesystem.security.impl;

import me.songt.purchasesystem.po.User;
import me.songt.purchasesystem.repository.UserRepository;
import me.songt.purchasesystem.security.TokenUtils;
import me.songt.purchasesystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.Base64;

/**
 * Created by tony on 2017/3/5.
 */
@Service
@Configurable
public class TokenUtilsImpl implements TokenUtils
{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @Override
    public String getToken(User user)
    {
        String plainToken = "";

        if (user != null)
        {
            String encodedToken = this.generateTokenByUser(user);
            user.setUserToken(encodedToken);
            userRepository.save(user);
            return encodedToken;
        }
        else
        {
            return null;
        }
    }

    @Override
    public boolean validate(String token)
    {
        long currentMillSeconds = System.currentTimeMillis();
        String plainToken = this.base64decode(token);
        String[] tokenInfo = parsePlainToken(plainToken);
        //int userId = Integer.valueOf(tokenInfo[0]);
        String userName = tokenInfo[1];
        String password = tokenInfo[2];
        long tokenMillSeconds = Long.valueOf(tokenInfo[3]);
        long timeElapsed = currentMillSeconds - tokenMillSeconds;
        //Token Life Time: 30 min
        if (timeElapsed > 1800000)
        {
            return false;
        }
        return userService.authenticateByUsernameAndPassword(userName, password) != null;
    }

    @Override
    public User getUserFromToken(String token)
    {
        String[] tokenInfo = this.parsePlainToken(this.base64decode(token));
        if (validate(token))
        {
            int userId = Integer.valueOf(tokenInfo[0]);
//            User foundUser = userRepository.findOne((long) userId);
            User foundUser = userRepository.findByuserId(userId);
            return foundUser;
        }
        return null;
    }

    private String generateTokenByUser(User user)
    {
        String plainToken = user.getUserId() + "|" + user.getUserName() + "|" + user.getUserPassword() + "|"
                + String.valueOf(System.currentTimeMillis());
        byte[] encodedBytes = Base64.getEncoder().encode(plainToken.getBytes());
        String encodedToken = new String(encodedBytes);
        //user.setUserToken(encodedToken);
        return encodedToken;
    }

    private String[] parsePlainToken(String plaintext)
    {
        return plaintext.split("\\|");
    }

    private String base64decode(String token)
    {
        return new String(Base64.getDecoder().decode(token.getBytes()));
    }
}
