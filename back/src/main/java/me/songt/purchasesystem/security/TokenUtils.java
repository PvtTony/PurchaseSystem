package me.songt.purchasesystem.security;

import me.songt.purchasesystem.po.User;

/**
 * Created by tony on 2017/3/5.
 */
public interface TokenUtils
{
    String getToken(User user);

    boolean validate(String token);

    User getUserFromToken(String token);
}
