package me.songt.purchasesystem.security;

import me.songt.purchasesystem.po.User;
import org.slf4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by tony on 2017/3/5.
 */
@Component
public class AuthenticationTokenProcessingFilter extends GenericFilterBean
{
    private final Logger logger = org.slf4j.LoggerFactory.getLogger(AuthenticationTokenProcessingFilter.class);
    private TokenUtils tokenUtils;
    private AuthenticationManager authenticationManager;

    public AuthenticationTokenProcessingFilter(AuthenticationManager authenticationManager, TokenUtils tokenUtils)
    {
        this.authenticationManager = authenticationManager;
        this.tokenUtils = tokenUtils;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add((GrantedAuthority) () -> "USER");

        //SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
//        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, getServletContext());
        Map<String, String[]> headParams = servletRequest.getParameterMap();

        String AUTH_HEAD_NAME = "Token";
        if (headParams.containsKey(AUTH_HEAD_NAME))
        {
            String token = headParams.get(AUTH_HEAD_NAME)[0];
            logger.debug(token);
            if (tokenUtils.validate(token))
            {
                User user = tokenUtils.getUserFromToken(token);
                logger.info("Name: " + user.getUserName());
                logger.info("Pass: " + user.getUserPassword());
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUserName(), user.getUserPassword(), authorities);
//                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUserName(), user.getUserPassword());
                /*logger.info((String) authenticationToken.getCredentials());
                logger.info((String) authenticationToken.getPrincipal());
                logger.info(authenticationToken.getName());*/
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) servletRequest));
                Authentication authentication = authenticationManager.authenticate(authenticationToken);
//                authentication.setAuthenticated(true);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
