package me.songt.purchasesystem.po;

import javax.persistence.*;

/**
 * Created by tony on 2017/2/23.
 * Supplier Type POJO
 */
@Entity
@Table(name = "supplier_type")
public class SupplierType
{
    private int supplierTypeId;
    private String supplierTypeName;

    @Id
    @Column(name = "supplier_type_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getSupplierTypeId()
    {
        return supplierTypeId;
    }

    public void setSupplierTypeId(int supplierTypeId)
    {
        this.supplierTypeId = supplierTypeId;
    }

    @Basic
    @Column(name = "supplier_type_name", nullable = false, length = 100)
    public String getSupplierTypeName()
    {
        return supplierTypeName;
    }

    public void setSupplierTypeName(String supplierTypeName)
    {
        this.supplierTypeName = supplierTypeName;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        SupplierType that = (SupplierType) o;

        if (supplierTypeId != that.supplierTypeId)
        {
            return false;
        }
        if (supplierTypeName != null ? !supplierTypeName.equals(that.supplierTypeName) : that.supplierTypeName != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = supplierTypeId;
        result = 31 * result + (supplierTypeName != null ? supplierTypeName.hashCode() : 0);
        return result;
    }
}
