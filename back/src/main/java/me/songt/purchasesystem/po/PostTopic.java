package me.songt.purchasesystem.po;

import javax.persistence.*;

/**
 * Created by tony on 2017/2/23.
 * Post Topic POJO
 */
@Entity
@Table(name = "post_topic")
public class PostTopic
{
    private int postTopicId;
    private String postTopicName;

    @Id
    @Column(name = "post_topic_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getPostTopicId()
    {
        return postTopicId;
    }

    public void setPostTopicId(int postTopicId)
    {
        this.postTopicId = postTopicId;
    }

    @Basic
    @Column(name = "post_topic_name", nullable = false, length = 200)
    public String getPostTopicName()
    {
        return postTopicName;
    }

    public void setPostTopicName(String postTopicName)
    {
        this.postTopicName = postTopicName;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        PostTopic postTopic = (PostTopic) o;

        if (postTopicId != postTopic.postTopicId)
        {
            return false;
        }
        if (postTopicName != null ? !postTopicName.equals(postTopic.postTopicName) : postTopic.postTopicName != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = postTopicId;
        result = 31 * result + (postTopicName != null ? postTopicName.hashCode() : 0);
        return result;
    }
}
