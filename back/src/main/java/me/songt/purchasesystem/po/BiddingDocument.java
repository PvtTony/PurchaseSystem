package me.songt.purchasesystem.po;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by AUG01 on 2017/3/15.
 */
@Entity
@Table(name = "bidding_document")
public class BiddingDocument {
    private int biddingId;
    private String biddingProjectName;
    private int biddingLotNum;
    private Timestamp biddingBeginTime;
    private Timestamp biddingEndTime;
    private String biddingMethod;
    private String biddingAttachments;
    private String biddingNum;
    private String biddingRemark;

    @Id
    @Column(name = "bidding_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getBiddingId() {
        return biddingId;
    }

    public void setBiddingId(int biddingId) {
        this.biddingId = biddingId;
    }

    @Basic
    @Column(name = "bidding_project_name", nullable = false, length = 255)
    public String getBiddingProjectName() {
        return biddingProjectName;
    }

    public void setBiddingProjectName(String biddingProjectName) {
        this.biddingProjectName = biddingProjectName;
    }

    @Basic
    @Column(name = "bidding_lot_num", nullable = false, precision = 0)
    public int getBiddingLotNum() {
        return biddingLotNum;
    }

    public void setBiddingLotNum(int biddingLotNum) {
        this.biddingLotNum = biddingLotNum;
    }

    @Basic
    @Column(name = "bidding_begin_time", nullable = false)
    public Timestamp getBiddingBeginTime() {
        return biddingBeginTime;
    }

    public void setBiddingBeginTime(Timestamp biddingBeginTime) {
        this.biddingBeginTime = biddingBeginTime;
    }

    @Basic
    @Column(name = "bidding_end_time", nullable = false)
    public Timestamp getBiddingEndTime() {
        return biddingEndTime;
    }

    public void setBiddingEndTime(Timestamp biddingEndTime) {
        this.biddingEndTime = biddingEndTime;
    }

    @Basic
    @Column(name = "bidding_method", nullable = false, length = 255)
    public String getBiddingMethod() {
        return biddingMethod;
    }

    public void setBiddingMethod(String biddingMethod) {
        this.biddingMethod = biddingMethod;
    }

    @Basic
    @Column(name = "bidding_attachments", nullable = true, length = 2147483647)
    public String getBiddingAttachments() {
        return biddingAttachments;
    }

    public void setBiddingAttachments(String biddingAttachments) {
        this.biddingAttachments = biddingAttachments;
    }

    @Basic
    @Column(name = "bidding_num", nullable = true, length = 100)
    public String getBiddingNum() {
        return biddingNum;
    }

    public void setBiddingNum(String biddingNum) {
        this.biddingNum = biddingNum;
    }

    @Basic
    @Column(name = "bidding_remark", nullable = true, length = 2147483647)
    public String getBiddingRemark() {
        return biddingRemark;
    }

    public void setBiddingRemark(String biddingRemark) {
        this.biddingRemark = biddingRemark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BiddingDocument that = (BiddingDocument) o;

        if (biddingId != that.biddingId) return false;
        if (biddingLotNum != that.biddingLotNum) return false;
        if (biddingProjectName != null ? !biddingProjectName.equals(that.biddingProjectName) : that.biddingProjectName != null)
            return false;
        if (biddingBeginTime != null ? !biddingBeginTime.equals(that.biddingBeginTime) : that.biddingBeginTime != null)
            return false;
        if (biddingEndTime != null ? !biddingEndTime.equals(that.biddingEndTime) : that.biddingEndTime != null)
            return false;
        if (biddingMethod != null ? !biddingMethod.equals(that.biddingMethod) : that.biddingMethod != null)
            return false;
        if (biddingAttachments != null ? !biddingAttachments.equals(that.biddingAttachments) : that.biddingAttachments != null)
            return false;
        if (biddingNum != null ? !biddingNum.equals(that.biddingNum) : that.biddingNum != null) return false;
        if (biddingRemark != null ? !biddingRemark.equals(that.biddingRemark) : that.biddingRemark != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = biddingId;
        result = 31 * result + (biddingProjectName != null ? biddingProjectName.hashCode() : 0);
        result = 31 * result + biddingLotNum;
        result = 31 * result + (biddingBeginTime != null ? biddingBeginTime.hashCode() : 0);
        result = 31 * result + (biddingEndTime != null ? biddingEndTime.hashCode() : 0);
        result = 31 * result + (biddingMethod != null ? biddingMethod.hashCode() : 0);
        result = 31 * result + (biddingAttachments != null ? biddingAttachments.hashCode() : 0);
        result = 31 * result + (biddingNum != null ? biddingNum.hashCode() : 0);
        result = 31 * result + (biddingRemark != null ? biddingRemark.hashCode() : 0);
        return result;
    }
}
