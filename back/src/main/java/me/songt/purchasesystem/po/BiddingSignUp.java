package me.songt.purchasesystem.po;

import javax.persistence.*;

/**
 * Created by AUG01 on 2017/3/16.
 */
@Entity
@Table(name = "bidding_sign_up")
public class BiddingSignUp {
    private Integer biddingId;
    private Integer supplierId;
    private String tel;
    private String authPerson;
    private String ip;
    private String depositPaymentMethod;
    private int id;

    @Basic
    @Column(name = "bidding_id", nullable = true)
    public Integer getBiddingId() {
        return biddingId;
    }

    public void setBiddingId(Integer biddingId) {
        this.biddingId = biddingId;
    }

    @Basic
    @Column(name = "supplier_id", nullable = true)
    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    @Basic
    @Column(name = "tel", nullable = true, length = 50)
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Basic
    @Column(name = "auth_person", nullable = true, length = 50)
    public String getAuthPerson() {
        return authPerson;
    }

    public void setAuthPerson(String authPerson) {
        this.authPerson = authPerson;
    }

    @Basic
    @Column(name = "ip", nullable = true, length = 50)
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Basic
    @Column(name = "deposit_payment_method", nullable = true, length = 50)
    public String getDepositPaymentMethond() {
        return depositPaymentMethod;
    }

    public void setDepositPaymentMethond(String depositPaymentMethond) {
        this.depositPaymentMethod = depositPaymentMethond;
    }

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BiddingSignUp that = (BiddingSignUp) o;

        if (id != that.id) return false;
        if (biddingId != null ? !biddingId.equals(that.biddingId) : that.biddingId != null) return false;
        if (supplierId != null ? !supplierId.equals(that.supplierId) : that.supplierId != null) return false;
        if (tel != null ? !tel.equals(that.tel) : that.tel != null) return false;
        if (authPerson != null ? !authPerson.equals(that.authPerson) : that.authPerson != null) return false;
        if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;
        if (depositPaymentMethod != null ? !depositPaymentMethod.equals(that.depositPaymentMethod) : that.depositPaymentMethod != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = biddingId != null ? biddingId.hashCode() : 0;
        result = 31 * result + (supplierId != null ? supplierId.hashCode() : 0);
        result = 31 * result + (tel != null ? tel.hashCode() : 0);
        result = 31 * result + (authPerson != null ? authPerson.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (depositPaymentMethod != null ? depositPaymentMethod.hashCode() : 0);
        result = 31 * result + id;
        return result;
    }
}
