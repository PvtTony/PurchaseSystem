package me.songt.purchasesystem.po;

import javax.persistence.*;

/**
 * Created by tony on 2017/2/23.
 * Post Level POJO
 */
@Entity
@Table(name = "post_level")
public class PostLevel
{
    private int postLevelId;
    private String postName;
    private int postHasparent;
    private String postPath;
    private int postDepth;

    @Id
    @Column(name = "post_level_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getPostLevelId()
    {
        return postLevelId;
    }

    public void setPostLevelId(int postLevelId)
    {
        this.postLevelId = postLevelId;
    }

    @Basic
    @Column(name = "post_name", nullable = false, length = 200)
    public String getPostName()
    {
        return postName;
    }

    public void setPostName(String postName)
    {
        this.postName = postName;
    }

    @Basic
    @Column(name = "post_hasparent", nullable = false)
    public int getPostHasparent()
    {
        return postHasparent;
    }

    public void setPostHasparent(int postHasparent)
    {
        this.postHasparent = postHasparent;
    }

    @Basic
    @Column(name = "post_path", nullable = false, length = 200)
    public String getPostPath()
    {
        return postPath;
    }

    public void setPostPath(String postPath)
    {
        this.postPath = postPath;
    }

    @Basic
    @Column(name = "post_depth", nullable = false)
    public int getPostDepth()
    {
        return postDepth;
    }

    public void setPostDepth(int postDepth)
    {
        this.postDepth = postDepth;
    }

    public PostLevel()
    {
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        PostLevel postLevel = (PostLevel) o;

        if (postLevelId != postLevel.postLevelId)
        {
            return false;
        }
        if (postHasparent != postLevel.postHasparent)
        {
            return false;
        }
        if (postDepth != postLevel.postDepth)
        {
            return false;
        }
        if (postName != null ? !postName.equals(postLevel.postName) : postLevel.postName != null)
        {
            return false;
        }
        if (postPath != null ? !postPath.equals(postLevel.postPath) : postLevel.postPath != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = postLevelId;
        result = 31 * result + (postName != null ? postName.hashCode() : 0);
        result = 31 * result + postHasparent;
        result = 31 * result + (postPath != null ? postPath.hashCode() : 0);
        result = 31 * result + postDepth;
        return result;
    }
}
