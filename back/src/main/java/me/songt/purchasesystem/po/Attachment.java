package me.songt.purchasesystem.po;

import javax.persistence.*;

/**
 * Created by AUG01 on 2017/3/18.
 */
@Entity
public class Attachment {
    private int attachmentId;
    private String attachmentDescribe;
    private String attachmentPath;
    private int attachmentBiddingId;

    @Id
    @Column(name = "attachment_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(int attachmentId) {
        this.attachmentId = attachmentId;
    }

    @Basic
    @Column(name = "attachment_describe")
    public String getAttachmentDescribe() {
        return attachmentDescribe;
    }

    public void setAttachmentDescribe(String attachmentDescribe) {
        this.attachmentDescribe = attachmentDescribe;
    }

    @Basic
    @Column(name = "attachment_path")
    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    @Basic
    @Column(name = "attachment_bidding_id")
    public int getAttachmentBiddingId() {
        return attachmentBiddingId;
    }

    public void setAttachmentBiddingId(int attachmentBiddingId) {
        this.attachmentBiddingId = attachmentBiddingId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attachment that = (Attachment) o;

        if (attachmentId != that.attachmentId) return false;
        if (attachmentBiddingId != that.attachmentBiddingId) return false;
        if (attachmentDescribe != null ? !attachmentDescribe.equals(that.attachmentDescribe) : that.attachmentDescribe != null)
            return false;
        if (attachmentPath != null ? !attachmentPath.equals(that.attachmentPath) : that.attachmentPath != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = attachmentId;
        result = 31 * result + (attachmentDescribe != null ? attachmentDescribe.hashCode() : 0);
        result = 31 * result + (attachmentPath != null ? attachmentPath.hashCode() : 0);
        result = 31 * result + attachmentBiddingId;
        return result;
    }
}
