package me.songt.purchasesystem.po;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by AUG01 on 2017/3/17.
 */
@Entity
@Table(name = "supplier")
public class Supplier {
    private int supplierId;
    private String supplierName;
    private String supplierCompanyName;
    private Timestamp supplierBirthdate;
    private int supplierTypeId;
    private String supplierRegisterNum;
    private String supplierRegisterCapital;
    private String supplierOrgNum;
    private String supplierTaxNum;
    private String supplierContact;
    private String supplierContactTel;
    private String supplierEmail;
    private String supplierAddress;
    private String supplierQq;
    private int supplierSignId;
    private int supplierLicenseImageId;
    private String supplierScope;
    private String supplierLawRep;
    private int supplierIsbanned;
    private int supplierIspassed;
    private String supplierPassword;

    @Id
    @Column(name = "supplier_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    @Basic
    @Column(name = "supplier_name")
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @Basic
    @Column(name = "supplier_company_name")
    public String getSupplierCompanyName() {
        return supplierCompanyName;
    }

    public void setSupplierCompanyName(String supplierCompanyName) {
        this.supplierCompanyName = supplierCompanyName;
    }

    @Basic
    @Column(name = "supplier_birthdate")
    public Timestamp getSupplierBirthdate() {
        return supplierBirthdate;
    }

    public void setSupplierBirthdate(Timestamp supplierBirthdate) {
        this.supplierBirthdate = supplierBirthdate;
    }

    @Basic
    @Column(name = "supplier_type_id")
    public int getSupplierTypeId() {
        return supplierTypeId;
    }

    public void setSupplierTypeId(int supplierTypeId) {
        this.supplierTypeId = supplierTypeId;
    }

    @Basic
    @Column(name = "supplier_register_num")
    public String getSupplierRegisterNum() {
        return supplierRegisterNum;
    }

    public void setSupplierRegisterNum(String supplierRegisterNum) {
        this.supplierRegisterNum = supplierRegisterNum;
    }

    @Basic
    @Column(name = "supplier_register_capital")
    public String getSupplierRegisterCapital() {
        return supplierRegisterCapital;
    }

    public void setSupplierRegisterCapital(String supplierRegisterCapital) {
        this.supplierRegisterCapital = supplierRegisterCapital;
    }

    @Basic
    @Column(name = "supplier_org_num")
    public String getSupplierOrgNum() {
        return supplierOrgNum;
    }

    public void setSupplierOrgNum(String supplierOrgNum) {
        this.supplierOrgNum = supplierOrgNum;
    }

    @Basic
    @Column(name = "supplier_tax_num")
    public String getSupplierTaxNum() {
        return supplierTaxNum;
    }

    public void setSupplierTaxNum(String supplierTaxNum) {
        this.supplierTaxNum = supplierTaxNum;
    }

    @Basic
    @Column(name = "supplier_contact")
    public String getSupplierContact() {
        return supplierContact;
    }

    public void setSupplierContact(String supplierContact) {
        this.supplierContact = supplierContact;
    }

    @Basic
    @Column(name = "supplier_contact_tel")
    public String getSupplierContactTel() {
        return supplierContactTel;
    }

    public void setSupplierContactTel(String supplierContactTel) {
        this.supplierContactTel = supplierContactTel;
    }

    @Basic
    @Column(name = "supplier_email")
    public String getSupplierEmail() {
        return supplierEmail;
    }

    public void setSupplierEmail(String supplierEmail) {
        this.supplierEmail = supplierEmail;
    }

    @Basic
    @Column(name = "supplier_address")
    public String getSupplierAddress() {
        return supplierAddress;
    }

    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    @Basic
    @Column(name = "supplier_qq")
    public String getSupplierQq() {
        return supplierQq;
    }

    public void setSupplierQq(String supplierQq) {
        this.supplierQq = supplierQq;
    }

    @Basic
    @Column(name = "supplier_sign_id")
    public int getSupplierSignId() {
        return supplierSignId;
    }

    public void setSupplierSignId(int supplierSignId) {
        this.supplierSignId = supplierSignId;
    }

    @Basic
    @Column(name = "supplier_license_image_id")
    public int getSupplierLicenseImageId() {
        return supplierLicenseImageId;
    }

    public void setSupplierLicenseImageId(int supplierLicenseImageId) {
        this.supplierLicenseImageId = supplierLicenseImageId;
    }

    @Basic
    @Column(name = "supplier_scope")
    public String getSupplierScope() {
        return supplierScope;
    }

    public void setSupplierScope(String supplierScope) {
        this.supplierScope = supplierScope;
    }

    @Basic
    @Column(name = "supplier_law_rep")
    public String getSupplierLawRep() {
        return supplierLawRep;
    }

    public void setSupplierLawRep(String supplierLawRep) {
        this.supplierLawRep = supplierLawRep;
    }

    @Basic
    @Column(name = "supplier_isbanned")
    public int getSupplierIsbanned() {
        return supplierIsbanned;
    }

    public void setSupplierIsbanned(int supplierIsbanned) {
        this.supplierIsbanned = supplierIsbanned;
    }

    @Basic
    @Column(name = "supplier_ispassed")
    public int getSupplierIspassed() {
        return supplierIspassed;
    }

    public void setSupplierIspassed(int supplierIspassed) {
        this.supplierIspassed = supplierIspassed;
    }

    @Basic
    @Column(name = "supplier_password")
    public String getSupplierPassword() {
        return supplierPassword;
    }

    public void setSupplierPassword(String supplierPassword) {
        this.supplierPassword = supplierPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Supplier supplier = (Supplier) o;

        if (supplierId != supplier.supplierId) return false;
        if (supplierTypeId != supplier.supplierTypeId) return false;
        if (supplierSignId != supplier.supplierSignId) return false;
        if (supplierLicenseImageId != supplier.supplierLicenseImageId) return false;
        if (supplierIsbanned != supplier.supplierIsbanned) return false;
        if (supplierIspassed != supplier.supplierIspassed) return false;
        if (supplierName != null ? !supplierName.equals(supplier.supplierName) : supplier.supplierName != null)
            return false;
        if (supplierCompanyName != null ? !supplierCompanyName.equals(supplier.supplierCompanyName) : supplier.supplierCompanyName != null)
            return false;
        if (supplierBirthdate != null ? !supplierBirthdate.equals(supplier.supplierBirthdate) : supplier.supplierBirthdate != null)
            return false;
        if (supplierRegisterNum != null ? !supplierRegisterNum.equals(supplier.supplierRegisterNum) : supplier.supplierRegisterNum != null)
            return false;
        if (supplierRegisterCapital != null ? !supplierRegisterCapital.equals(supplier.supplierRegisterCapital) : supplier.supplierRegisterCapital != null)
            return false;
        if (supplierOrgNum != null ? !supplierOrgNum.equals(supplier.supplierOrgNum) : supplier.supplierOrgNum != null)
            return false;
        if (supplierTaxNum != null ? !supplierTaxNum.equals(supplier.supplierTaxNum) : supplier.supplierTaxNum != null)
            return false;
        if (supplierContact != null ? !supplierContact.equals(supplier.supplierContact) : supplier.supplierContact != null)
            return false;
        if (supplierContactTel != null ? !supplierContactTel.equals(supplier.supplierContactTel) : supplier.supplierContactTel != null)
            return false;
        if (supplierEmail != null ? !supplierEmail.equals(supplier.supplierEmail) : supplier.supplierEmail != null)
            return false;
        if (supplierAddress != null ? !supplierAddress.equals(supplier.supplierAddress) : supplier.supplierAddress != null)
            return false;
        if (supplierQq != null ? !supplierQq.equals(supplier.supplierQq) : supplier.supplierQq != null) return false;
        if (supplierScope != null ? !supplierScope.equals(supplier.supplierScope) : supplier.supplierScope != null)
            return false;
        if (supplierLawRep != null ? !supplierLawRep.equals(supplier.supplierLawRep) : supplier.supplierLawRep != null)
            return false;
        if (supplierPassword != null ? !supplierPassword.equals(supplier.supplierPassword) : supplier.supplierPassword != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = supplierId;
        result = 31 * result + (supplierName != null ? supplierName.hashCode() : 0);
        result = 31 * result + (supplierCompanyName != null ? supplierCompanyName.hashCode() : 0);
        result = 31 * result + (supplierBirthdate != null ? supplierBirthdate.hashCode() : 0);
        result = 31 * result + supplierTypeId;
        result = 31 * result + (supplierRegisterNum != null ? supplierRegisterNum.hashCode() : 0);
        result = 31 * result + (supplierRegisterCapital != null ? supplierRegisterCapital.hashCode() : 0);
        result = 31 * result + (supplierOrgNum != null ? supplierOrgNum.hashCode() : 0);
        result = 31 * result + (supplierTaxNum != null ? supplierTaxNum.hashCode() : 0);
        result = 31 * result + (supplierContact != null ? supplierContact.hashCode() : 0);
        result = 31 * result + (supplierContactTel != null ? supplierContactTel.hashCode() : 0);
        result = 31 * result + (supplierEmail != null ? supplierEmail.hashCode() : 0);
        result = 31 * result + (supplierAddress != null ? supplierAddress.hashCode() : 0);
        result = 31 * result + (supplierQq != null ? supplierQq.hashCode() : 0);
        result = 31 * result + supplierSignId;
        result = 31 * result + supplierLicenseImageId;
        result = 31 * result + (supplierScope != null ? supplierScope.hashCode() : 0);
        result = 31 * result + (supplierLawRep != null ? supplierLawRep.hashCode() : 0);
        result = 31 * result + supplierIsbanned;
        result = 31 * result + supplierIspassed;
        result = 31 * result + (supplierPassword != null ? supplierPassword.hashCode() : 0);
        return result;
    }
}
