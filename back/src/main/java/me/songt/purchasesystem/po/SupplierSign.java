package me.songt.purchasesystem.po;

import javax.persistence.*;

/**
 * Created by tony on 2017/2/23.
 * Supplier Sign POJO
 */
@Entity
@Table(name = "supplier_sign")
public class SupplierSign
{
    private int supplierSignId;
    private String supplierAuthName;
    private String supplierDeposit;

    @Id
    @Column(name = "supplier_sign_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getSupplierSignId()
    {
        return supplierSignId;
    }

    public void setSupplierSignId(int supplierSignId)
    {
        this.supplierSignId = supplierSignId;
    }

    @Basic
    @Column(name = "supplier_auth_name", nullable = false, length = 200)
    public String getSupplierAuthName()
    {
        return supplierAuthName;
    }

    public void setSupplierAuthName(String supplierAuthName)
    {
        this.supplierAuthName = supplierAuthName;
    }

    @Basic
    @Column(name = "supplier_deposit", nullable = true, length = 200)
    public String getSupplierDeposit()
    {
        return supplierDeposit;
    }

    public void setSupplierDeposit(String supplierDeposit)
    {
        this.supplierDeposit = supplierDeposit;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        SupplierSign that = (SupplierSign) o;

        if (supplierSignId != that.supplierSignId)
        {
            return false;
        }
        if (supplierAuthName != null ? !supplierAuthName.equals(that.supplierAuthName) : that.supplierAuthName != null)
        {
            return false;
        }
        if (supplierDeposit != null ? !supplierDeposit.equals(that.supplierDeposit) : that.supplierDeposit != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = supplierSignId;
        result = 31 * result + (supplierAuthName != null ? supplierAuthName.hashCode() : 0);
        result = 31 * result + (supplierDeposit != null ? supplierDeposit.hashCode() : 0);
        return result;
    }
}
