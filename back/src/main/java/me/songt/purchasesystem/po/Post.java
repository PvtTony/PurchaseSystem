package me.songt.purchasesystem.po;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by tony on 2017/2/26.
 * Post POJO
 */
@Entity
@Table(name = "post")
public class Post
{
    private int postId;
    private String postText;
    private String postTitle;
    private String postPurchase;
    private Integer postType;
    private int postLevelId;
    private Integer postTopicId;
    private Timestamp postPublishTime;
    private Integer postImageId;
    private Integer postDeleted;
    private String postAttachments;

    @Id
    @Column(name = "post_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getPostId()
    {
        return postId;
    }

    public void setPostId(int postId)
    {
        this.postId = postId;
    }

    @Basic
    @Column(name = "post_title", nullable = false, length = 255)
    public String getPostTitle()
    {
        return postTitle;
    }

    public void setPostTitle(String postTitle)
    {
        this.postTitle = postTitle;
    }

    @Basic
    @Column(name = "post_text", nullable = false, length = -1)
    public String getPostText()
    {
        return postText;
    }

    public void setPostText(String postText)
    {
        this.postText = postText;
    }

    @Basic
    @Column(name = "post_purchase", nullable = true, length = 200)
    public String getPostPurchase()
    {
        return postPurchase;
    }

    public void setPostPurchase(String postPurchase)
    {
        this.postPurchase = postPurchase;
    }

    @Basic
    @Column(name = "post_type", nullable = true)
    public Integer getPostType()
    {
        return postType;
    }

    public void setPostType(Integer postType)
    {
        this.postType = postType;
    }

    @Basic
    @Column(name = "post_level_id", nullable = false)
    public int getPostLevelId()
    {
        return postLevelId;
    }

    public void setPostLevelId(int postLevelId)
    {
        this.postLevelId = postLevelId;
    }

    @Basic
    @Column(name = "post_topic_id", nullable = true)
    public Integer getPostTopicId()
    {
        return postTopicId;
    }

    public void setPostTopicId(Integer postTopicId)
    {
        this.postTopicId = postTopicId;
    }

    @Basic
    @Column(name = "post_publish_time", nullable = false)
    public Timestamp getPostPublishTime()
    {
        return postPublishTime;
    }

    public void setPostPublishTime(Timestamp postPublishTime)
    {
        this.postPublishTime = postPublishTime;
    }

    @Basic
    @Column(name = "post_image_id", nullable = true)
    public Integer getPostImageId()
    {
        return postImageId;
    }

    public void setPostImageId(Integer postImageId)
    {
        this.postImageId = postImageId;
    }

    @Basic
    @Column(name = "post_deleted", nullable = true)
    public Integer getPostDeleted()
    {
        return postDeleted;
    }

    public void setPostDeleted(Integer postDeleted)
    {
        this.postDeleted = postDeleted;
    }

    @Basic
    @Column(name = "post_attachments", nullable = true, length = 2147483647)
    public String getPostAttachments()
    {
        return postAttachments;
    }

    public void setPostAttachments(String postAttachments)
    {
        this.postAttachments = postAttachments;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Post post = (Post) o;

        if (postId != post.postId)
        {
            return false;
        }
        if (postLevelId != post.postLevelId)
        {
            return false;
        }
        if (postText != null ? !postText.equals(post.postText) : post.postText != null)
        {
            return false;
        }
        if (postPurchase != null ? !postPurchase.equals(post.postPurchase) : post.postPurchase != null)
        {
            return false;
        }
        if (postType != null ? !postType.equals(post.postType) : post.postType != null)
        {
            return false;
        }
        if (postTopicId != null ? !postTopicId.equals(post.postTopicId) : post.postTopicId != null)
        {
            return false;
        }
        if (postPublishTime != null ? !postPublishTime.equals(post.postPublishTime) : post.postPublishTime != null)
        {
            return false;
        }
        if (postImageId != null ? !postImageId.equals(post.postImageId) : post.postImageId != null)
        {
            return false;
        }
        if (postDeleted != null ? !postDeleted.equals(post.postDeleted) : post.postDeleted != null)
        {
            return false;
        }
        return postAttachments != null ? postAttachments.equals(post.postAttachments) : post.postAttachments == null;
    }

    @Override
    public int hashCode()
    {
        int result = postId;
        result = 31 * result + (postText != null ? postText.hashCode() : 0);
        result = 31 * result + (postPurchase != null ? postPurchase.hashCode() : 0);
        result = 31 * result + (postType != null ? postType.hashCode() : 0);
        result = 31 * result + postLevelId;
        result = 31 * result + (postTopicId != null ? postTopicId.hashCode() : 0);
        result = 31 * result + (postPublishTime != null ? postPublishTime.hashCode() : 0);
        result = 31 * result + (postImageId != null ? postImageId.hashCode() : 0);
        result = 31 * result + (postDeleted != null ? postDeleted.hashCode() : 0);
        result = 31 * result + (postAttachments != null ? postAttachments.hashCode() : 0);
        return result;
    }
}
