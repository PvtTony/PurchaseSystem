package me.songt.purchasesystem.po;

import javax.persistence.*;

/**
 * Created by tony on 2017/3/4.
 */
@Entity
@Table(name = "user_data")
public class User
{
    private int userId;
    private String userName;
    private String userPassword;
    private String userToken;
    private int userEnabled;

    @Id
    @Column(name = "user_id", nullable = false)
    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    @Basic
    @Column(name = "user_name", nullable = false, length = 200)
    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    @Basic
    @Column(name = "user_password", nullable = false, length = 200)
    public String getUserPassword()
    {
        return userPassword;
    }

    public void setUserPassword(String userPassword)
    {
        this.userPassword = userPassword;
    }

    @Basic
    @Column(name = "user_token", nullable = false, length = 200)
    public String getUserToken()
    {
        return userToken;
    }

    public void setUserToken(String userToken)
    {
        this.userToken = userToken;
    }

    @Basic
    @Column(name = "user_enabled", nullable = false)
    public int getUserEnabled()
    {
        return userEnabled;
    }

    public void setUserEnabled(int userEnabled)
    {
        this.userEnabled = userEnabled;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        User user = (User) o;

        if (userId != user.userId)
        {
            return false;
        }
        if (userName != null ? !userName.equals(user.userName) : user.userName != null)
        {
            return false;
        }
        if (userPassword != null ? !userPassword.equals(user.userPassword) : user.userPassword != null)
        {
            return false;
        }
        return userToken != null ? userToken.equals(user.userToken) : user.userToken == null;
    }

    @Override
    public int hashCode()
    {
        int result = userId;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (userPassword != null ? userPassword.hashCode() : 0);
        result = 31 * result + (userToken != null ? userToken.hashCode() : 0);
        return result;
    }
}
