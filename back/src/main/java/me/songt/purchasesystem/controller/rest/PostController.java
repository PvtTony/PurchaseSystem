package me.songt.purchasesystem.controller.rest;

import me.songt.purchasesystem.po.Post;
import me.songt.purchasesystem.service.PostLevelService;
import me.songt.purchasesystem.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by tony on 2017/2/23.
 * Post Controller
 */
@RestController
@CrossOrigin
public class PostController
{
    @Autowired
    private PostService postService;
    @Autowired
    private PostLevelService postLevelService;
/*
    @RequestMapping(value = "/api/post", method = RequestMethod.GET)
    public List<Post> getAll()
    {
        return (List<Post>) postService.getAllPosts();
    }*/

    //分页查询所有post
    @RequestMapping(value = "/api/post", method = RequestMethod.GET)
    public Page<Post> getAll(@RequestParam(name = "pageIndex", defaultValue = "0") int pageIndex,
                             @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                             @RequestParam(name = "sortProperty", defaultValue = "postPublishTime") String sortProperty,
                             @RequestParam(name = "sortDesc", defaultValue = "1") int desc) {
        return postService.getAllPosts(pageIndex, pageSize, sortProperty, desc == 1);
    }

    //查询post总数（用于分页栏）
    @RequestMapping(value = "api/post/count", method = RequestMethod.GET)
    public long getPostCount() {
        return postService.getPostCount();
    }

    @RequestMapping(value = "/api/post/level/{levelId}", method = RequestMethod.GET)
    public Page<Post> getSpecificLevelPosts(@PathVariable("levelId") int levelId,
                                            @RequestParam(name = "pageIndex", defaultValue = "0") int pageIndex,
                                            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                                            @RequestParam(name = "sortProperty", defaultValue = "postPublishTime") String sortProperty,
                                            @RequestParam(name = "sortDesc", defaultValue = "1") int desc)
    {
        return postService.findSpecificLevelPosts(postLevelService.findChildLevelIds(postLevelService.getChildLevel(levelId)),
                pageIndex,
                pageSize, sortProperty, desc == 1);
    }

    @RequestMapping(value = "/api/post/id/{postId}", method = RequestMethod.GET)
    public Post getSpecificIdPost(@PathVariable("postId") int postId)
    {
        return postService.findSpecificPost(postId);
    }

    //@Secured("ROLE_USER")
    @RequestMapping(value = "/api/post", method = RequestMethod.POST)
    public int addPost(@RequestParam(name = "postTitle") String postTitle,
                        @RequestParam(name = "postText") String postText,
                        @RequestParam(name = "postPurchase", defaultValue = "null") String postPurchase,
                        @RequestParam(name = "postType", defaultValue = "0") int postType,
                        @RequestParam(name = "postLevelId") int postLevelId,
                        @RequestParam(name = "postTopicId", defaultValue = "0") int postTopicId,
                        @RequestParam(name = "postImageId", defaultValue = "0") int postImageId,
                        @RequestParam(name = "postAttachments", defaultValue = "null") String postAttachments)
    {
        Post post = new Post();
        post.setPostTitle(postTitle);
        post.setPostText(postText);
        post.setPostPurchase(postPurchase.equals("null") ? null : postPurchase);
        post.setPostType(postType != 0 ? postType : null);
        post.setPostLevelId(postLevelId);
        post.setPostTopicId(postTopicId != 0 ? postTopicId : null);
        post.setPostImageId(postImageId != 0 ? postImageId : null);
        post.setPostAttachments(postAttachments.equals("null") ? null : postAttachments);

        post.setPostDeleted(0);
        post.setPostPublishTime(new Timestamp(System.currentTimeMillis()));

        postService.addPost(post);
        return 1;
    }

    @RequestMapping(value = "/api/post/id/{postId}", method = RequestMethod.PUT)
    public int updatePost(@PathVariable("postId") int postId,
                                        @RequestParam(name = "postTitle") String postTitle,
                                        @RequestParam(name = "postText") String postText,
                                        @RequestParam(name = "postPurchase", defaultValue = "null") String postPurchase,
                                        @RequestParam(name = "postType", defaultValue = "0") int postType,
                                        @RequestParam(name = "postLevelId") int postLevelId,
                                        @RequestParam(name = "postTopicId", defaultValue = "0") int postTopicId,
                                        @RequestParam(name = "postImageId", defaultValue = "0") int postImageId,
                                        @RequestParam(name = "postAttachments", defaultValue = "null") String postAttachments)
    {
        Post alterPost = new Post();
        alterPost.setPostTitle(postTitle);
        alterPost.setPostText(postText);
        alterPost.setPostPurchase(postPurchase.equals("null") ? null : postPurchase);
        alterPost.setPostType(postType != 0 ? postType : null);
        alterPost.setPostLevelId(postLevelId);
        alterPost.setPostTopicId(postTopicId != 0 ? postTopicId : null);
        alterPost.setPostImageId(postImageId != 0 ? postImageId : null);
        alterPost.setPostAttachments(postAttachments.equals("null") ? null : postAttachments);

        alterPost.setPostDeleted(0);
        alterPost.setPostPublishTime(new Timestamp(System.currentTimeMillis()));
        postService.updateSpecificPost(postId, alterPost);
        return 1;
    }

   /* @RequestMapping(value = "/api/post/alter/{postId}", method = RequestMethod.POST)
    public int updatePost(@PathVariable("postId") int postId,
                          @RequestParam(name = "postTitle") String postTitle,
                          @RequestParam(name = "postText") String postText,
                          @RequestParam(name = "postPurchase", defaultValue = "null") String postPurchase,
                          @RequestParam(name = "postType", defaultValue = "0") int postType,
                          @RequestParam(name = "postLevelId") int postLevelId,
                          @RequestParam(name = "postTopicId", defaultValue = "0") int postTopicId,
                          @RequestParam(name = "postImageId", defaultValue = "0") int postImageId,
                          @RequestParam(name = "postAttachments", defaultValue = "null") String postAttachments)
    {
        Post alterPost = new Post();
        alterPost.setPostTitle(postTitle);
        alterPost.setPostText(postText);
        alterPost.setPostPurchase(postPurchase.equals("null") ? null : postPurchase);
        alterPost.setPostType(postType != 0 ? postType : null);
        alterPost.setPostLevelId(postLevelId);
        alterPost.setPostTopicId(postTopicId != 0 ? postTopicId : null);
        alterPost.setPostImageId(postImageId != 0 ? postImageId : null);
        alterPost.setPostAttachments(postAttachments.equals("null") ? null : postAttachments);

        alterPost.setPostDeleted(0);
        alterPost.setPostPublishTime(new Timestamp(System.currentTimeMillis()));
        postService.updateSpecificPost(postId, alterPost);
        return 1;
    }*/

    @RequestMapping(value = "/api/post/trashed", method = RequestMethod.GET)
    public ResponseEntity<?> findTrashedPosts(@RequestParam(name = "pageIndex", defaultValue = "0") int pageIndex,
                                              @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                                              @RequestParam(name = "sortProperty", defaultValue = "postPublishTime") String sortProperty,
                                              @RequestParam(name = "sortDesc", defaultValue = "1") int desc)
    {
        return new ResponseEntity<>(postService.findTrashedPosts(pageIndex, pageSize, sortProperty, desc == 1), HttpStatus.OK);
    }

    /*@RequestMapping(value = "/api/post/id/{postId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> moveToTrash(@PathVariable("postId") int postId)
    {
        postService.moveToTrash(postId);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }*/

    @RequestMapping(value = "/api/post/id/{postId}", method = RequestMethod.DELETE)
    public int moveToTrash(@PathVariable("postId") int postId)
    {
        postService.moveToTrash(postId);
        return 1;
    }

    @RequestMapping(value = "/api/post/trashed/{postId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> permanentRemovePost(@PathVariable("postId") int postId)
    {
        postService.removeSpecificPost(postId);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
