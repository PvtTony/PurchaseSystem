package me.songt.purchasesystem.controller.rest;

import me.songt.purchasesystem.po.Supplier;
import me.songt.purchasesystem.po.SupplierType;
import me.songt.purchasesystem.service.SupplierService;
import me.songt.purchasesystem.service.SupplierTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.GET;
import java.sql.Timestamp;


/**
 * Created by AUG01 on 2017/3/16.
 */
@RestController
@CrossOrigin
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    @Autowired
    SupplierTypeService supplierTypeService;

    //查询所有的supplierType
    @RequestMapping(value = "/api/supplier/type", method = RequestMethod.GET)
    public Page<SupplierType> getAllSupplierType()
    {
        return supplierTypeService.getSupplierType();
    }

    //通过id查询supplierType
    @RequestMapping(value = "/api/supplier/type/{supplierTypeId}", method = RequestMethod.GET)
    public SupplierType findSupplierType(@PathVariable(value = "supplierTypeId")int supplierTypeId)
    {
        return supplierTypeService.findSupplierType(supplierTypeId);
    }

    //通过id查询一个supplier
    @RequestMapping(value = "/api/supplier/id/{supplierId}", method = RequestMethod.GET)
    public Supplier getOneSupplier(@PathVariable(value = "supplierId")int supplierId )
    {
        return supplierService.findOneSupplier(supplierId);
    }

    //供应商注册
    @RequestMapping(value = "/api/supplier", method = RequestMethod.POST)
    public Supplier supplierRegister(@RequestParam(value = "supplierName", defaultValue = "null")String supplierName ,
                                     @RequestParam(value = "supplierCompanyName", defaultValue = "null")String supplierCompanyName ,
                                     @RequestParam(value = "supplierTypeId", defaultValue = "0")int supplierTypeId ,
                                     @RequestParam(value = "supplierRegisterNum", defaultValue = "null")String supplierRegisterNum ,
                                     @RequestParam(value = "supplierRegisterCapital", defaultValue = "null")String supplierRegisterCapital ,
                                     @RequestParam(value = "supplierOrgNum", defaultValue = "null")String supplierOrgNum ,
                                     @RequestParam(value = "supplierTaxNum", defaultValue = "null")String supplierTaxNum ,
                                     @RequestParam(value = "supplierContact", defaultValue = "null")String supplierContact ,
                                     @RequestParam(value = "supplierContactTel", defaultValue = "null")String supplierContactTel ,
                                     @RequestParam(value = "supplierEmail", defaultValue = "null")String supplierEmail ,
                                     @RequestParam(value = "supplierAddress", defaultValue = "null")String supplierAddress ,
                                     @RequestParam(value = "supplierQq", defaultValue = "null")String supplierQq ,
                                     @RequestParam(value = "supplierScope", defaultValue = "null")String supplierScope ,
                                     @RequestParam(value = "supplierBirthdate", defaultValue = "null")String supplierBirthdateStr ,
                                     @RequestParam(value = "supplierPassword", defaultValue = "null")String supplierPassword ,
                                     @RequestParam(value = "supplierLawRep", defaultValue = "null")String supplierLawRep )
//                                     @RequestParam(value = "supplierSignId", defaultValue = "null")int supplierSignId ,
//                                     @RequestParam(value = "supplierLicenseImageId", defaultValue = "null")int supplierLicenseImageId
    {
        Timestamp supplierBirthdate = Timestamp.valueOf(supplierBirthdateStr);

        Supplier supplier = new Supplier();

        supplier.setSupplierName(supplierName);
        supplier.setSupplierCompanyName(supplierCompanyName);
        supplier.setSupplierTypeId(supplierTypeId);
        supplier.setSupplierRegisterNum(supplierRegisterNum);
        supplier.setSupplierRegisterCapital(supplierRegisterCapital);
        supplier.setSupplierOrgNum(supplierOrgNum);
        supplier.setSupplierTaxNum(supplierTaxNum);
        supplier.setSupplierContact(supplierContact);
        supplier.setSupplierContactTel(supplierContactTel);
        supplier.setSupplierEmail(supplierEmail);
        supplier.setSupplierAddress(supplierAddress);
        supplier.setSupplierQq(supplierQq);
        supplier.setSupplierScope(supplierScope);
        supplier.setSupplierLawRep(supplierLawRep);
        supplier.setSupplierBirthdate(supplierBirthdate);
        supplier.setSupplierPassword(supplierPassword);
//        supplier.setSupplierIsbanned(0);
//        supplier.setSupplierIspassed(0);

        supplier.setSupplierLicenseImageId(0);
        supplier.setSupplierSignId(0);

        return supplierService.supplierRegister(supplier);
    }

    //供应商信息修改
    @RequestMapping(value = "/api/supplier/alter/{supplierId}", method = RequestMethod.PUT)
    public Supplier supplierRegister(@PathVariable(value = "supplierId")int supplierId ,
                                     @RequestParam(value = "supplierName", defaultValue = "null")String supplierName ,
                                     @RequestParam(value = "supplierCompanyName", defaultValue = "null")String supplierCompanyName ,
                                     @RequestParam(value = "supplierTypeId", defaultValue = "0")int supplierTypeId ,
                                     @RequestParam(value = "supplierRegisterNum", defaultValue = "null")String supplierRegisterNum ,
                                     @RequestParam(value = "supplierRegisterCapital", defaultValue = "null")String supplierRegisterCapital ,
                                     @RequestParam(value = "supplierOrgNum", defaultValue = "null")String supplierOrgNum ,
                                     @RequestParam(value = "supplierTaxNum", defaultValue = "null")String supplierTaxNum ,
                                     @RequestParam(value = "supplierContact", defaultValue = "null")String supplierContact ,
                                     @RequestParam(value = "supplierContactTel", defaultValue = "null")String supplierContactTel ,
                                     @RequestParam(value = "supplierEmail", defaultValue = "null")String supplierEmail ,
                                     @RequestParam(value = "supplierAddress", defaultValue = "null")String supplierAddress ,
                                     @RequestParam(value = "supplierQq", defaultValue = "null")String supplierQq ,
                                     @RequestParam(value = "supplierBirthdate", defaultValue = "null")String supplierBirthdateStr ,
                                     @RequestParam(value = "supplierScope", defaultValue = "null")String supplierScope ,
                                     @RequestParam(value = "supplierPassword", defaultValue = "null")String supplierPassword ,
                                     @RequestParam(value = "supplierLawRep", defaultValue = "null")String supplierLawRep )
//                                     @RequestParam(value = "supplierSignId", defaultValue = "null")int supplierSignId ,
//                                     @RequestParam(value = "supplierLicenseImageId", defaultValue = "null")int supplierLicenseImageId
    {
        Timestamp supplierBirthdate = Timestamp.valueOf(supplierBirthdateStr);

        System.out.println("\n"+supplierBirthdateStr+"\n"+supplierBirthdate+"\n");

        Supplier supplier = new Supplier();

        supplier.setSupplierId(supplierId);
        supplier.setSupplierName(supplierName);
        supplier.setSupplierCompanyName(supplierCompanyName);
        supplier.setSupplierTypeId(supplierTypeId);
        supplier.setSupplierRegisterNum(supplierRegisterNum);
        supplier.setSupplierRegisterCapital(supplierRegisterCapital);
        supplier.setSupplierOrgNum(supplierOrgNum);
        supplier.setSupplierTaxNum(supplierTaxNum);
        supplier.setSupplierContact(supplierContact);
        supplier.setSupplierContactTel(supplierContactTel);
        supplier.setSupplierEmail(supplierEmail);
        supplier.setSupplierAddress(supplierAddress);
        supplier.setSupplierQq(supplierQq);
        supplier.setSupplierBirthdate(supplierBirthdate);
        supplier.setSupplierScope(supplierScope);
        supplier.setSupplierLawRep(supplierLawRep);
        supplier.setSupplierPassword(supplierPassword);

        supplier.setSupplierLicenseImageId(0);
        supplier.setSupplierSignId(0);

        return supplierService.alterSupplier(supplier);
    }
}
