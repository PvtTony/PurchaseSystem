package me.songt.purchasesystem.controller.rest;

import me.songt.purchasesystem.po.Attachment;
import me.songt.purchasesystem.po.Image;
import me.songt.purchasesystem.repository.AttachmentRepository;
import me.songt.purchasesystem.repository.ImageRepository;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Paths;
import java.security.MessageDigest;

/**
 * Created by tony on 2017/2/25.
 * File Upload Controller
 */
@RestController
@CrossOrigin
public class FileController
{
    //Logger
    private Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private AttachmentRepository attachmentRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ServletContext context;

    @Value("${filePath}")
    private String tmpFilePath;

    //Create MD5 from the MultipartFile
    @NotNull
    private String createMd5OfMultipartFile(final MultipartFile file) throws Exception
    {
        StringBuilder builder = new StringBuilder();
        MessageDigest md5 = MessageDigest.getInstance("md5");
        InputStream stream = file.getInputStream();
        int available = stream.available();
        byte[] bytes = new byte[available];
        md5.update(bytes);
        for (byte byt : md5.digest())
        {
            builder.append(String.format("%02X", byt));
        }
        return builder.toString();
    }

    @RequestMapping(value = "/api/file/reUpload/{biddingId}", method = RequestMethod.PUT)
    public ResponseEntity<?> reUploadFile(@RequestParam(value = "uploadFile", required = true) MultipartFile uploadFile,
                                        @PathVariable(value = "biddingId") int biddingId)
    {
//        Attachment attachment = new Attachment();
//        attachment.setAttachmentBiddingId(biddingId);
        Attachment attachment = attachmentRepository.findByattachmentBiddingId(biddingId);
        try
        {
            String fileName = uploadFile.getOriginalFilename();
            String fileExtension = fileName.substring(fileName.indexOf("."));
            String relativePath = "/uploadedfiles/";
            String fileMd5 = this.createMd5OfMultipartFile(uploadFile);
//            String absoultePath = context.getRealPath(relativePath);
            String absoultePath = tmpFilePath+relativePath;
            String storageFileName = fileMd5 + fileExtension;
            String filePath = Paths.get(absoultePath, storageFileName).toString();
            attachment.setAttachmentPath(relativePath + storageFileName);
            attachment.setAttachmentDescribe(fileName);

            //Check if the folder exists
            File fileDirectory = new File(absoultePath);
            if (!fileDirectory.exists())
            {
                if (!fileDirectory.mkdir())
                {
                    logger.debug("Folder Create Failed. Path: " + absoultePath);
                }
            }
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
            stream.write(uploadFile.getBytes());
            stream.close();

        } catch (FileNotFoundException e)
        {
            logger.warn("FileNotFoundException: " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IOException e)
        {
            logger.warn("IOException: " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        attachmentRepository.save(attachment);
        return new ResponseEntity<>(attachment, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/file/upload/{biddingId}", method = RequestMethod.POST)
    public ResponseEntity<?> uploadFile(@RequestParam(value = "uploadFile", required = true) MultipartFile uploadFile,
                                        @PathVariable(value = "biddingId") int biddingId)
    {
        Attachment attachment = new Attachment();
        attachment.setAttachmentBiddingId(biddingId);
        try
        {
            String fileName = uploadFile.getOriginalFilename();
            String fileExtension = fileName.substring(fileName.indexOf("."));
            String relativePath = "/uploadedfiles/";
            String fileMd5 = this.createMd5OfMultipartFile(uploadFile);
//            String absoultePath = context.getRealPath(relativePath);
            String absoultePath = tmpFilePath+relativePath;
            String storageFileName = fileMd5 + fileExtension;
            String filePath = Paths.get(absoultePath, storageFileName).toString();
            attachment.setAttachmentPath(relativePath + storageFileName);
            attachment.setAttachmentDescribe(fileName);

            //Check if the folder exists
            File fileDirectory = new File(absoultePath);
            if (!fileDirectory.exists())
            {
                if (!fileDirectory.mkdir())
                {
                    logger.debug("Folder Create Failed. Path: " + absoultePath);
                }
            }
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
            stream.write(uploadFile.getBytes());
            stream.close();

        } catch (FileNotFoundException e)
        {
            logger.warn("FileNotFoundException: " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IOException e)
        {
            logger.warn("IOException: " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        attachmentRepository.save(attachment);
        return new ResponseEntity<>(attachment, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/image/upload/{supplierId}", method = RequestMethod.POST)
    public ResponseEntity<?> uploadImage(@RequestParam(value = "uploadFile", required = true) MultipartFile uploadImage ,
                                         @PathVariable(value = "supplierId") int id)
    {
        Image image = new Image();
        image.setSupplierId(id);
        try
        {
            String imageName = uploadImage.getOriginalFilename();
            String fileExtension = imageName.substring(imageName.indexOf("."));
            String relativePath = "/uploadedimages/";
            String fileMd5 = this.createMd5OfMultipartFile(uploadImage);
//            String absoultePath = context.getRealPath(relativePath);
            String absoultePath = tmpFilePath+relativePath;
            String storageFileName = fileMd5 + fileExtension;
            String filePath = Paths.get(absoultePath, storageFileName).toString();
//            image.setAttachmentPath(relativePath + storageFileName);
//            attachment.setAttachmentDescribe(imageName);
            image.setImageUrl(relativePath + storageFileName);
            image.setImageDescribe(imageName);

            //Check if the folder exists
            File fileDirectory = new File(absoultePath);
            if (!fileDirectory.exists())
            {
                if (!fileDirectory.mkdir())
                {
                    logger.debug("Folder Create Failed. Path: " + absoultePath);
                }
            }
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
            stream.write(uploadImage.getBytes());
            stream.close();

        } catch (FileNotFoundException e)
        {
            logger.warn("FileNotFoundException: " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IOException e)
        {
            logger.warn("IOException: " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        imageRepository.save(image);
        return new ResponseEntity<>(image, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/image/reUpload/{supplierId}", method = RequestMethod.PUT)
    public ResponseEntity<?> reUploadImage(@RequestParam(value = "uploadFile", required = true) MultipartFile uploadImage ,
                                         @PathVariable(value = "supplierId") int id)
    {
//        Image image = new Image();
        Image image = imageRepository.findBysupplierId(id);
        try
        {
            String imageName = uploadImage.getOriginalFilename();
            String fileExtension = imageName.substring(imageName.indexOf("."));
            String relativePath = "/uploadedimages/";
            String fileMd5 = this.createMd5OfMultipartFile(uploadImage);
//            String absoultePath = context.getRealPath(relativePath);
            String absoultePath = tmpFilePath+relativePath;
            System.out.println("\n"+absoultePath+"\n");
            String storageFileName = fileMd5 + fileExtension;
            String filePath = Paths.get(absoultePath, storageFileName).toString();
//            image.setAttachmentPath(relativePath + storageFileName);
//            attachment.setAttachmentDescribe(imageName);
            image.setImageUrl(relativePath + storageFileName);
            image.setImageDescribe(imageName);
            image.setSupplierId(id);

            //Check if the folder exists
            File fileDirectory = new File(absoultePath);
            if (!fileDirectory.exists())
            {
                if (!fileDirectory.mkdir())
                {
                    logger.debug("Folder Create Failed. Path: " + absoultePath);
                }
            }
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
            stream.write(uploadImage.getBytes());
            stream.close();

        } catch (FileNotFoundException e)
        {
            logger.warn("FileNotFoundException: " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IOException e)
        {
            logger.warn("IOException: " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        imageRepository.save(image);
        return new ResponseEntity<>(image, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/file/{fileId}", method = RequestMethod.GET)
    public ResponseEntity<?> getAttachmentInfoById(@PathVariable("fileId") int id)
    {
        Attachment attachment = attachmentRepository.findByattachmentId(id);
        if (attachment != null)
        {
            return new ResponseEntity<>(attachment, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/api/fileInfo/{biddingId}", method = RequestMethod.GET)
    public ResponseEntity<?> getAttachmentInfoBybiddingId(@PathVariable("biddingId") int id)
    {
        Attachment attachment = attachmentRepository.findByattachmentBiddingId(id);
        if (attachment != null)
        {
            return new ResponseEntity<>(attachment, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/api/image/{imageId}", method = RequestMethod.GET)
    public ResponseEntity<?> getImageById(@PathVariable("imageId") int id, HttpServletResponse response)
    {
        Image image = imageRepository.findByimageId(id);

        if (image != null)
        {
            try
            {
                response.sendRedirect(image.getImageUrl());
                return new ResponseEntity<>(image, HttpStatus.OK);
            } catch (IOException e)
            {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
