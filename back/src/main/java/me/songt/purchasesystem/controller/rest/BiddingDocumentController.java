package me.songt.purchasesystem.controller.rest;

import me.songt.purchasesystem.po.BiddingDocument;
import me.songt.purchasesystem.service.BiddingDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

/**
 * Created by tony on 2017/3/5.
 */
@RestController
@CrossOrigin
public class BiddingDocumentController
{
    @Autowired
    private BiddingDocumentService biddingDocumentService;

    //分页查询所有标书
    @RequestMapping(value = "/api/biddingdoc", method = RequestMethod.GET)
    public Page<BiddingDocument> findAllDoc(@RequestParam(name = "pageIndex", defaultValue = "0") int pageIndex,
                                            @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
                                            @RequestParam(name = "sortProperty", defaultValue = "biddingBeginTime") String sortProperty,
                                            @RequestParam(name = "sortDesc", defaultValue = "1") boolean desc) {
        return biddingDocumentService.getAllDocuments(pageIndex, pageSize, sortProperty, desc);
    }

    //通过id查询一个标书的报名情况
//    @RequestMapping(value = "/api/biddingdoc/signUpCount/{biddingId}" , method = RequestMethod.GET)

    //通过id查询一个标书
    @RequestMapping(value = "/api/biddingdoc/id/{biddingId}", method = RequestMethod.GET)
    public BiddingDocument getOneBiddingDoc(@PathVariable("biddingId") int biddingId){
        return biddingDocumentService.getOneDocument(biddingId);
    }

    //通过查询是否存在这个biddingNum(标书编号)
    @RequestMapping(value = "/api/biddingdoc/isBiddingNumExist/{biddingNum}", method = RequestMethod.GET)
    public boolean isBiddingNumExist(@PathVariable("biddingNum") String biddingNum){
        return biddingDocumentService.isBiddingNumExist(biddingNum);
    }

    //查询标书总数
    @RequestMapping(value = "/api/biddingdoc/count", method = RequestMethod.GET)
    public long getDocCount() {
        return biddingDocumentService.getDocumentCount();
    }

    //添加一个标书
    @RequestMapping(value = "/api/biddingdoc/adddoc", method = RequestMethod.POST)
    public BiddingDocument addDoc(//@RequestParam(name = "biddingId", defaultValue = "0") int biddingId,
                       @RequestParam(name = "biddingProjectName", defaultValue = "null") String biddingProjectName,
                       @RequestParam(name = "biddingLotNum", defaultValue = "1") int biddingLotNum,
                       @RequestParam(name = "biddingMethod", defaultValue = "null") String biddingMethod,
                       @RequestParam(name = "biddingBeginTime", defaultValue = "null") String biddingBeginTimeStr,
                       @RequestParam(name = "biddingEndTime", defaultValue = "null") String biddingEndTimeStr,
                       @RequestParam(name = "biddingAttachments", defaultValue = "null") String biddingAttachments,
                       @RequestParam(name = "biddingNum", defaultValue = "null") String biddingNum,
                       @RequestParam(name = "biddingRemark", defaultValue = "null") String biddingRemark) {
        Timestamp biddingBeginTime = Timestamp.valueOf(biddingBeginTimeStr);
        Timestamp biddingEndTime = Timestamp.valueOf(biddingEndTimeStr);

        BiddingDocument biddingDocument = new BiddingDocument();
        //biddingDocument.setBiddingId(biddingId);
        biddingDocument.setBiddingMethod(biddingMethod);
        biddingDocument.setBiddingProjectName(biddingProjectName);
        biddingDocument.setBiddingLotNum(biddingLotNum);
        biddingDocument.setBiddingBeginTime(biddingBeginTime);
        biddingDocument.setBiddingEndTime(biddingEndTime);
        biddingDocument.setBiddingAttachments(biddingAttachments);
        biddingDocument.setBiddingNum(biddingNum);
        biddingDocument.setBiddingRemark(biddingRemark);

        return biddingDocumentService.addDocument(biddingDocument);

    }

    //删除一个标书
    @RequestMapping(value = "/api/biddingdoc/trashed/{biddingId}", method = RequestMethod.DELETE)
    public int removeBiddingDoc(@PathVariable(name = "biddingId") int biddingId) {
        biddingDocumentService.removeDocument(biddingId);
        return 1;
    }

    //修改一个标书
    @RequestMapping(value = "/api/biddingdoc/alter/{biddingId}", method = RequestMethod.PUT)
    public BiddingDocument updateBiddingDoc(@PathVariable(name = "biddingId") int biddingId,
                                 @RequestParam(name = "biddingProjectName", defaultValue = "null") String biddingProjectName,
                                 @RequestParam(name = "biddingLotNum", defaultValue = "1") int biddingLotNum,
                                 @RequestParam(name = "biddingMethod", defaultValue = "null") String biddingMethod,
                                 @RequestParam(name = "biddingBeginTime", defaultValue = "null") String biddingBeginTimeStr,
                                 @RequestParam(name = "biddingEndTime", defaultValue = "null") String biddingEndTimeStr,
                                 @RequestParam(name = "biddingAttachments", defaultValue = "null") String biddingAttachments,
                                 @RequestParam(name = "biddingNum", defaultValue = "null") String biddingNum,
                                 @RequestParam(name = "biddingRemark", defaultValue = "null") String biddingRemark) {

        Timestamp biddingBeginTime = Timestamp.valueOf(biddingBeginTimeStr);
        Timestamp biddingEndTime = Timestamp.valueOf(biddingEndTimeStr);

        BiddingDocument alterBiddingDocument = new BiddingDocument();
        alterBiddingDocument.setBiddingProjectName(biddingProjectName);
        alterBiddingDocument.setBiddingLotNum(biddingLotNum);
        alterBiddingDocument.setBiddingBeginTime(biddingBeginTime);
        alterBiddingDocument.setBiddingEndTime(biddingEndTime);
        alterBiddingDocument.setBiddingAttachments(biddingAttachments);
        alterBiddingDocument.setBiddingNum(biddingNum);
        alterBiddingDocument.setBiddingRemark(biddingRemark);

        return biddingDocumentService.updateDocument(biddingId, alterBiddingDocument);
    }

}
