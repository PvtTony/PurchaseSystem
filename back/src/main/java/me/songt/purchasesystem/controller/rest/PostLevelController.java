package me.songt.purchasesystem.controller.rest;

import me.songt.purchasesystem.service.PostLevelService;
import me.songt.purchasesystem.vo.PostLevelTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by tony on 2017/2/24.
 * Post Level Controller
 */
@RestController
public class PostLevelController
{
    @Autowired
    private PostLevelService postLevelService;

    @RequestMapping(value = "/api/postlevel", method = RequestMethod.GET)
    private List<PostLevelTree> findAllLevelData()
    {
        return postLevelService.findAllLevel();
    }

    @RequestMapping(value = "/api/postlevel/{levelId}", method = RequestMethod.GET)
    private List<PostLevelTree> findChildLevelData(@PathVariable("levelId") int levelId)
    {
        return postLevelService.findSpecificChildLevel(levelId);
    }
}
