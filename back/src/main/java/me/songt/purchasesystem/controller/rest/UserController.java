package me.songt.purchasesystem.controller.rest;

import me.songt.purchasesystem.po.User;
import me.songt.purchasesystem.security.TokenUtils;
import me.songt.purchasesystem.service.UserService;
import me.songt.purchasesystem.vo.TokenDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tony on 2017/3/5.
 */
@RestController
public class UserController
{
    @Autowired
    private UserService userService;

    @Autowired
    private TokenUtils tokenUtils;

    @RequestMapping(value = "/api/user", method = RequestMethod.GET)
    public ResponseEntity<?> getToken(@RequestParam("username") String username, @RequestParam("password") String password)
    {
        User user = userService.authenticateByUsernameAndPassword(username, password);
        String token = "";
        if (user != null)
        {
            token = tokenUtils.getToken(user);
        }

        TokenDetail detail = new TokenDetail();
        detail.setUserName(user.getUserName());
        detail.setUserId(user.getUserId());
        detail.setToken(token);
        return new ResponseEntity<Object>(detail, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/user", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestParam("username") String username, @RequestParam("password") String password)
    {
        User newUser = userService.register(username, password);
        return new ResponseEntity<>(tokenUtils.getToken(newUser), HttpStatus.OK);
    }
}
