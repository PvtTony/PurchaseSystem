package me.songt.purchasesystem.controller;

import me.songt.purchasesystem.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by tony on 2016/12/10.
 * Simple Test Controller
 */
@Controller
public class SimpleController
{
    @Autowired
    private PostService postService;

    @RequestMapping("/")
    public String index(Model model)
    {
        model.addAttribute("test", "Hello World");
        model.addAttribute("posts", postService.getAllPosts());
        return "index";
    }
}
