package me.songt.purchasesystem.controller.rest;

import me.songt.purchasesystem.po.BiddingSignUp;
import me.songt.purchasesystem.service.BiddingSignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by AUG01 on 2017/3/16.
 */
@RestController
@CrossOrigin
public class BiddingSignUpController {

    @Autowired
    private BiddingSignUpService biddingSignUpService;

    //通过biddingId查询报名总数
    @RequestMapping(value = "/api/biddingSignUp/count",method = RequestMethod.GET)
    public long signUpNum(@RequestParam(value = "biddingId", defaultValue = "0")int biddingId){
        return biddingSignUpService.signUpNum(biddingId);
    }

    //报名
    @RequestMapping(value = "/api/biddingSignUp", method = RequestMethod.POST)
    public BiddingSignUp biddingSignUp(@RequestParam(value = "biddingId", defaultValue = "0") int biddingId,
                                       @RequestParam(value = "supplierId", defaultValue = "0") int supplierId,
                                       @RequestParam(value = "tel",defaultValue = "null")String tel,
                                       @RequestParam(value = "authPerson",defaultValue = "null")String authPerson,
                                       @RequestParam(value = "ip",defaultValue = "null")String ip,
                                       @RequestParam(value = "depositPaymentMethond", defaultValue = "null")String depositPaymentMethond)
    {
        BiddingSignUp biddingSignUp = new BiddingSignUp();
        biddingSignUp.setBiddingId(biddingId);
        biddingSignUp.setSupplierId(supplierId);
        biddingSignUp.setTel(tel);
        biddingSignUp.setAuthPerson(authPerson);
        biddingSignUp.setIp(ip);
        biddingSignUp.setDepositPaymentMethond(depositPaymentMethond);

        return biddingSignUpService.signUp(biddingSignUp);
    }
}
