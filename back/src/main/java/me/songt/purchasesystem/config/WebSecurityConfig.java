package me.songt.purchasesystem.config;

import me.songt.purchasesystem.security.AuthenticationTokenProcessingFilter;
import me.songt.purchasesystem.security.RestAuthenticationEntryPoint;
import me.songt.purchasesystem.security.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Created by tony on 2017/3/2.
 * Spring Security Configuration
 */
@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "me.songt.purchasesystem")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter
{

    @Autowired
    private TokenUtils tokenUtils;

    /*@Autowired
    private DataSource dataSource;*/

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.
                csrf().disable();
        http.
                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.
                exceptionHandling().authenticationEntryPoint(new RestAuthenticationEntryPoint());
        http.
                authorizeRequests().
                mvcMatchers(HttpMethod.POST, "/api/post").fullyAuthenticated().
                mvcMatchers(HttpMethod.PUT, "/api/post/id/**").fullyAuthenticated().
                mvcMatchers(HttpMethod.DELETE, "/api/post/id/**").fullyAuthenticated().
                mvcMatchers(HttpMethod.DELETE, "/api/post/trashed/**").fullyAuthenticated().
                mvcMatchers(HttpMethod.POST, "/api/biddingdoc/adddoc").fullyAuthenticated().
                mvcMatchers(HttpMethod.DELETE, "/api/biddingdoc/trashed/**").fullyAuthenticated().
                mvcMatchers(HttpMethod.PUT, "/api/biddingdoc/alter/**").fullyAuthenticated().
//                mvcMatchers(HttpMethod.POST, "/api/biddingSignUp").fullyAuthenticated().
//                mvcMatchers(HttpMethod.POST, "/api/supplier/register").fullyAuthenticated().
//                mvcMatchers(HttpMethod.PUT, "/api/supplier/alter/**").fullyAuthenticated().
                anyRequest().permitAll();

        http.
                addFilterBefore(new AuthenticationTokenProcessingFilter(authenticationManager(), tokenUtils),
                        UsernamePasswordAuthenticationFilter.class);
        http.httpBasic();
    }

   /* @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {

    }*/
}
