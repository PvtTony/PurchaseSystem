package me.songt.purchasesystem.config;

import me.songt.purchasesystem.repository.UserRepository;
import me.songt.purchasesystem.service.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;

/**
 * Created by tony on 2017/3/7.
 */
@Configuration
public class GlobalAuthenticationConfig extends GlobalAuthenticationConfigurerAdapter
{

    @Autowired
    private UserRepository userRepository;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception
    {
//        super.init(auth);
        auth.userDetailsService(new UserDetailsServiceImpl(userRepository));
    }
}
