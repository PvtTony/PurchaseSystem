package me.songt.purchasesystem.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by tony on 2017/2/25.
 * Global Spring Boot Application Configuration Class
 */
@Configuration
public class AppConfiguration extends WebMvcConfigurerAdapter
{
    @Value("${filePath}")
    String filePath;

    //Global CORS Configuration, used in development phrase
    //Maybe removed in future.
    @Bean
    public WebMvcConfigurer corsConfigurer()
    {
        return new WebMvcConfigurerAdapter()
        {
            @Override
            public void addCorsMappings(CorsRegistry registry)
            {
                registry.addMapping("/api/**");
            }
        };
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //添加静态资源的访问
        registry.addResourceHandler("/uploadedimages/**").addResourceLocations("file:///"+filePath+"/uploadedimages/");
        registry.addResourceHandler("/uploadedfiles/**").addResourceLocations("file:///"+filePath+"/uploadedfiles/");
//        super.addResourceHandlers(registry);
    }
}
