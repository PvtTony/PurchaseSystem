package me.songt.purchasesystem.service.impl;

import me.songt.purchasesystem.po.Post;
import me.songt.purchasesystem.repository.PostRepository;
import me.songt.purchasesystem.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * Created by tony on 2016/12/10.
 * PostService Implement Class
 */
@Service
public class PostServiceImpl implements PostService
{
    @Autowired
    private PostRepository postRepository;

    public PostServiceImpl()
    {
    }

    @Override
    public Collection<Post> getAllPosts()
    {
        return (Collection<Post>) postRepository.findAll();
    }

    //分页查询所有post
    @Override
    public Page<Post> getAllPosts(int pageIndex, int pageSize, String sortField, boolean desc) {
        Sort sort = new Sort(desc ? Sort.Direction.DESC : Sort.Direction.ASC, sortField);
        return postRepository.findAll(new PageRequest(pageIndex, pageSize, sort));
    }

    //分页查询所有post，并返回具体的PostType等属性
    @Override
    public Page<Post> getAllPostsDetail(int pageIndex, int pageSize, String sortField, boolean desc) {
        return null;
    }

    //查询post总数
    @Override
    public long getPostCount() {
        return postRepository.count();
    }

    /*@Override
    public Page<Post> findSpecificLevelPosts(List<Integer> levelIds, int pageIndex, int pageSize)
    {
        Sort sort = new Sort(Sort.Direction.DESC, "postPublishTime");
        Page<Post> postRtn = postRepository.findByPostDeletedEqualsAndPostLevelIdIn(0, levelIds,
                new PageRequest(pageIndex, pageSize, sort));
        return postRtn;
    }*/

    @Override
    public Page<Post> findSpecificLevelPosts(List<Integer> levelIds, int pageIndex, int pageSize, String sortField, boolean desc)
    {
        Sort sort = new Sort(desc ? Sort.Direction.DESC : Sort.Direction.ASC, sortField);
        return postRepository.findByPostDeletedEqualsAndPostLevelIdIn(0, levelIds, new PageRequest(pageIndex, pageSize, sort));
//        return null;
    }


    @Override
    public void addPost(Post post)
    {
        postRepository.save(post);
    }


    @Override
    public Post findSpecificPost(int postId)
    {
//        Sort sort = new Sort(desc ? Sort.Direction.DESC : Sort.Direction.ASC, sortField);
        return postRepository.findBypostId(postId);
    }

    @Override
    public void updateSpecificPost(int postId, Post alterPost)
    {
        Post post = postRepository.findBypostId(postId);
        post.setPostTitle(alterPost.getPostTitle());
        post.setPostText(alterPost.getPostText());
        post.setPostPurchase(alterPost.getPostPurchase());
        post.setPostType(alterPost.getPostType());
        post.setPostLevelId(alterPost.getPostLevelId());
        post.setPostTopicId(alterPost.getPostTopicId());
        post.setPostPublishTime(alterPost.getPostPublishTime());
        post.setPostImageId(alterPost.getPostImageId());
        post.setPostDeleted(alterPost.getPostDeleted());
        post.setPostAttachments(alterPost.getPostAttachments());
        postRepository.save(post);
    }

    @Override
    public void removeSpecificPost(int postId)
    {
        postRepository.delete((long) postId);
    }

    @Override
    public void moveToTrash(int postId)
    {
        Post post = postRepository.findBypostId(postId);
        post.setPostDeleted(1);
        postRepository.save(post);

    }

    @Override
    public Page<Post> findTrashedPosts(int pageIndex, int pageSize, String sortField, boolean desc)
    {
        Sort sort = new Sort(desc ? Sort.Direction.DESC : Sort.Direction.ASC, sortField);
        return postRepository.findBypostDeletedEquals(1, new PageRequest(pageIndex, pageSize, sort));
    }
}
