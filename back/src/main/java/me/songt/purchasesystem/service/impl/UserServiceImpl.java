package me.songt.purchasesystem.service.impl;

import me.songt.purchasesystem.po.User;
import me.songt.purchasesystem.repository.UserRepository;
import me.songt.purchasesystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by tony on 2017/3/5.
 */
@Service
public class UserServiceImpl implements UserService
{
    @Autowired
    private UserRepository userRepository;

    @Override
    public User authenticateByUsernameAndPassword(String username, String password)
    {
        User user = userRepository.findByuserName(username);
        if (user != null && user.getUserPassword().equals(password))
        {
            return user;
        }
        return null;
    }

    @Override
    public User register(String username, String password)
    {
        User newUser = new User();
        newUser.setUserName(username);
        newUser.setUserPassword(password);
        newUser.setUserToken("");
        userRepository.save(newUser);
        return newUser;
    }
}
