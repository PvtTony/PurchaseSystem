package me.songt.purchasesystem.service;

import me.songt.purchasesystem.po.PostLevel;
import me.songt.purchasesystem.vo.PostLevelTree;

import java.util.Collection;
import java.util.List;

/**
 * Created by tony on 2017/2/23.
 * Post Level Service Interface
 */
public interface PostLevelService
{

    //public getAllLevel();
    Collection<PostLevel> getAllLevel();

    Collection<PostLevel> getChildLevel(int parentLevelId);

    List<PostLevelTree> findChildLevel(Collection<PostLevel> source);

    List<PostLevelTree> findAllLevel();

    List<PostLevelTree> findSpecificChildLevel(int parentLevelId);

    List<Integer> findChildLevelIds(Collection<PostLevel> source);

}
