package me.songt.purchasesystem.service;

import me.songt.purchasesystem.po.Post;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

/**
 * Created by tony on 2016/12/10.
 * Post Service Interface
 */
public interface PostService
{
    Collection<Post> getAllPosts();

//    Page<Post> findSpecificLevelPosts(List<Integer> levelIds, int pageIndex, int pageSize);

    Page<Post> findSpecificLevelPosts(List<Integer> levelIds, int pageIndex, int pageSize, String sortField, boolean desc);

    Page<Post> getAllPosts(int pageIndex, int pageSize, String sortField, boolean desc);

    Page<Post> getAllPostsDetail(int pageIndex, int pageSize, String sortField, boolean desc);

    long getPostCount();

    void addPost(Post post);

    Post findSpecificPost(int postId);

    void updateSpecificPost(int postId, Post alterPost);

    void removeSpecificPost(int postId);

    void moveToTrash(int postId);

    Page<Post> findTrashedPosts(int pageIndex, int pageSize, String sortField, boolean desc);
}
