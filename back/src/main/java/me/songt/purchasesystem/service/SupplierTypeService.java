package me.songt.purchasesystem.service;

import me.songt.purchasesystem.po.SupplierType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by AUG01 on 2017/3/17.
 */
public interface SupplierTypeService {
    Page<SupplierType> getSupplierType();

    SupplierType findSupplierType(int supplierTypeId);
}
