package me.songt.purchasesystem.service;

import me.songt.purchasesystem.po.BiddingSignUp;

/**
 * Created by AUG01 on 2017/3/16.
 */
public interface BiddingSignUpService
{
    BiddingSignUp signUp(BiddingSignUp biddingSignUp);

    long signUpNum(int biddingId);
}
