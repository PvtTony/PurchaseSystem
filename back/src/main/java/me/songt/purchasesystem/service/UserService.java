package me.songt.purchasesystem.service;

import me.songt.purchasesystem.po.User;
import org.springframework.stereotype.Service;

/**
 * Created by tony on 2017/3/5.
 */
@Service
public interface UserService
{
    User authenticateByUsernameAndPassword(String username, String password);

    User register(String username, String password);
}
