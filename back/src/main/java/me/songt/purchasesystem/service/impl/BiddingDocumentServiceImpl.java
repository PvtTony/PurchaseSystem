package me.songt.purchasesystem.service.impl;

import me.songt.purchasesystem.po.BiddingDocument;
import me.songt.purchasesystem.repository.BiddingDocumentRepository;
import me.songt.purchasesystem.service.BiddingDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by AUG01 on 2017/3/7.
 */
@Service
public class BiddingDocumentServiceImpl implements BiddingDocumentService {

    @Autowired
    private BiddingDocumentRepository biddingDocumentRepository;

    @Override
    public Page<BiddingDocument> getAllDocuments(int pageIndex, int pageSize, String sortField, boolean desc) {
        Sort sort = new Sort(desc ? Sort.Direction.DESC : Sort.Direction.ASC, sortField);
        return biddingDocumentRepository.findAll(new PageRequest(pageIndex, pageSize, sort));
    }

    @Override
    public long getDocumentCount() {
        return biddingDocumentRepository.count();
    }

    @Override
    public BiddingDocument addDocument(BiddingDocument document) {
        return biddingDocumentRepository.save(document);
    }

    @Override
    public void removeDocument(int biddingId) {
        BiddingDocument biddingDocument = new BiddingDocument();
        biddingDocument.setBiddingId(biddingId);
        biddingDocumentRepository.delete(biddingDocument);
    }

    @Override

    public BiddingDocument updateDocument(int biddingId, BiddingDocument alterDocument) {
        BiddingDocument biddingDocument = biddingDocumentRepository.findBybiddingId(biddingId);
        //BiddingDocument biddingDocument = new BiddingDocument();
        //biddingDocument.setBiddingId(alterDocument.getBiddingId());
        biddingDocument.setBiddingLotNum(alterDocument.getBiddingLotNum());
        biddingDocument.setBiddingMethod(alterDocument.getBiddingMethod());
        biddingDocument.setBiddingProjectName(alterDocument.getBiddingProjectName());
        biddingDocument.setBiddingBeginTime(alterDocument.getBiddingBeginTime());
        biddingDocument.setBiddingEndTime(alterDocument.getBiddingEndTime());
        biddingDocument.setBiddingAttachments(alterDocument.getBiddingAttachments());
        biddingDocument.setBiddingNum(alterDocument.getBiddingNum());
        biddingDocument.setBiddingRemark(alterDocument.getBiddingRemark());
        return biddingDocumentRepository.save(biddingDocument);
    }

    @Override
    public BiddingDocument getOneDocument(int biddingId) {
        BiddingDocument biddingDocument = biddingDocumentRepository.findBybiddingId(biddingId);
        return biddingDocument;
    }

    @Override
    public boolean isBiddingNumExist(String biddingNum) {
        BiddingDocument biddingDocument = biddingDocumentRepository.findBybiddingNum(biddingNum);
        if (biddingDocument == null){
            return false;
        }else {
            return true;
        }

    }

}
