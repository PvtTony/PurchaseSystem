package me.songt.purchasesystem.service;

import me.songt.purchasesystem.po.BiddingDocument;
import org.springframework.data.domain.Page;

/**
 * Created by tony on 2017/3/5.
 */
public interface BiddingDocumentService
{
    Page<BiddingDocument> getAllDocuments(int pageIndex, int pageSize, String sortField, boolean desc);

    long getDocumentCount();

    BiddingDocument addDocument(BiddingDocument document);

    void removeDocument(int biddingId);

    BiddingDocument updateDocument(int biddingId, BiddingDocument document);

    BiddingDocument getOneDocument(int biddingId);

    boolean isBiddingNumExist(String biddingNum);
}
