package me.songt.purchasesystem.service.impl;

import me.songt.purchasesystem.po.SupplierType;
import me.songt.purchasesystem.repository.SupplierTypeRepository;
import me.songt.purchasesystem.service.SupplierTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * Created by AUG01 on 2017/3/17.
 */
@Service
public class SupplierTypeServiceImpl implements SupplierTypeService {

    @Autowired
    private SupplierTypeRepository supplierTypeRepository;

    @Override
    public Page<SupplierType> getSupplierType() {
        Sort sort = new Sort(Sort.Direction.DESC, "supplierTypeId");
        Pageable pageable;
        return supplierTypeRepository.findAll(new PageRequest(1, 255, sort));
    }

    @Override
    public SupplierType findSupplierType(int supplierTypeId) {
        return supplierTypeRepository.findBysupplierTypeId(supplierTypeId);
    }

}
