package me.songt.purchasesystem.service;

import me.songt.purchasesystem.po.Supplier;

/**
 * Created by AUG01 on 2017/3/16.
 */
public interface SupplierService
{
    Supplier supplierRegister(Supplier supplier);

    Supplier alterSupplier(Supplier alterSupplier);

    Supplier findOneSupplier(int supplierId);
}
