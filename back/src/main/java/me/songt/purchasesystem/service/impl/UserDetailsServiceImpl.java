package me.songt.purchasesystem.service.impl;

import me.songt.purchasesystem.po.User;
import me.songt.purchasesystem.repository.UserRepository;
import me.songt.purchasesystem.vo.UserDetailsImpl;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tony on 2017/3/7.
 */
public class UserDetailsServiceImpl implements UserDetailsService
{

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException
    {
        User user = userRepository.findByuserName(s);
        if (user != null)
        {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

            return new UserDetailsImpl(user.getUserName(), user.getUserPassword(), authorities);
        }
        throw new UsernameNotFoundException("User: " + s + " not found.");
    }
}
