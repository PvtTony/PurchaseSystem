package me.songt.purchasesystem.service.impl;

import me.songt.purchasesystem.po.BiddingSignUp;
import me.songt.purchasesystem.repository.BiddingSignUpRepository;
import me.songt.purchasesystem.service.BiddingSignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by AUG01 on 2017/3/16.
 */
@Service
public class BiddingSignUpImpl implements BiddingSignUpService
{

    @Autowired
    private BiddingSignUpRepository biddingSignUpRepository;

    @Override
    public BiddingSignUp signUp(BiddingSignUp biddingSignUp)
    {
        return biddingSignUpRepository.save(biddingSignUp);
    }

    @Override
    public long signUpNum(int biddingId) {
        return biddingSignUpRepository.countBybiddingId(biddingId);
    }
}
