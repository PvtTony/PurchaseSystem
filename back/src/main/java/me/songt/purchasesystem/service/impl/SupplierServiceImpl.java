package me.songt.purchasesystem.service.impl;

import me.songt.purchasesystem.po.Supplier;
import me.songt.purchasesystem.repository.SupplierRepository;
import me.songt.purchasesystem.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by AUG01 on 2017/3/16.
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    SupplierRepository supplierRepository;

    @Override
    public Supplier supplierRegister(Supplier supplier) {
        return supplierRepository.save(supplier);
    }

    @Override
    public Supplier alterSupplier(Supplier alterSupplier) {
        Supplier supplier = new Supplier();

        supplier.setSupplierId(alterSupplier.getSupplierId());
        supplier.setSupplierName(alterSupplier.getSupplierName());
        supplier.setSupplierCompanyName(alterSupplier.getSupplierCompanyName());
        supplier.setSupplierTypeId(alterSupplier.getSupplierTypeId());
        supplier.setSupplierRegisterNum(alterSupplier.getSupplierRegisterNum());
        supplier.setSupplierRegisterCapital(alterSupplier.getSupplierRegisterCapital());
        supplier.setSupplierOrgNum(alterSupplier.getSupplierOrgNum());
        supplier.setSupplierTaxNum(alterSupplier.getSupplierTaxNum());
        supplier.setSupplierContact(alterSupplier.getSupplierContact());
        supplier.setSupplierContactTel(alterSupplier.getSupplierContactTel());
        supplier.setSupplierEmail(alterSupplier.getSupplierEmail());
        supplier.setSupplierAddress(alterSupplier.getSupplierAddress());
        supplier.setSupplierQq(alterSupplier.getSupplierQq());
        supplier.setSupplierBirthdate(alterSupplier.getSupplierBirthdate());
        supplier.setSupplierScope(alterSupplier.getSupplierScope());
        supplier.setSupplierLawRep(alterSupplier.getSupplierLawRep());
        supplier.setSupplierPassword(alterSupplier.getSupplierPassword());

        return supplierRepository.save(supplier);
    }

//    @Override
//    public Supplier findOneSupplier(int supplierId) {
//        return null;
//    }

    @Override
    public Supplier findOneSupplier(int supplierId) {
        return supplierRepository.findBysupplierId(supplierId);
    }
}
