package me.songt.purchasesystem.service.impl;

import me.songt.purchasesystem.po.PostLevel;
import me.songt.purchasesystem.repository.PostLevelRepository;
import me.songt.purchasesystem.service.PostLevelService;
import me.songt.purchasesystem.vo.PostLevelTree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by tony on 2017/2/23.
 * Post Level Service Implemented Class
 */
@Service
public class PostLevelServiceImpl implements PostLevelService
{
    //Logger
    private final Logger logger = LoggerFactory.getLogger(PostLevelServiceImpl.class);

    @Autowired
    private PostLevelRepository postLevelRepository;

    @Override
    public Collection<PostLevel> getAllLevel()
    {
        return (Collection<PostLevel>) postLevelRepository.findAll();
    }

    @Override
    public Collection<PostLevel> getChildLevel(int parentLevelId)
    {
        return (Collection<PostLevel>) postLevelRepository.findBypostHasparent(parentLevelId);
    }

    @Override
    public List<PostLevelTree> findChildLevel(Collection<PostLevel> source)
    {
        LinkedList<PostLevelTree> levelTree = new LinkedList<>();
        for (PostLevel levelItem : source)
        {
            PostLevelTree treeNode = new PostLevelTree(levelItem.getPostLevelId(),
                    levelItem.getPostName(), levelItem.getPostDepth());
            Collection<PostLevel> childs = this.getChildLevel(levelItem.getPostLevelId());
            //Find all child PostLevel Node recursively
            if (childs.size() != 0)
            {
                treeNode.setChildTree(findChildLevel(childs));
            }
            levelTree.add(treeNode);
        }
        return levelTree;
    }

    @Override
    public List<PostLevelTree> findAllLevel()
    {
        return findChildLevel((Collection<PostLevel>) postLevelRepository.findBypostDepth(0));
    }

    @Override
    public List<PostLevelTree> findSpecificChildLevel(int parentLevelId)
    {
        return findChildLevel(getChildLevel(parentLevelId));
    }

    @Override
    public List<Integer> findChildLevelIds(Collection<PostLevel> source)
    {
        ArrayList<Integer> childLevelId = new ArrayList<>();
        for (PostLevel levelItem : source)
        {
            //Log the child level id (Prompted in the CLI)
            //Application.getLogger().info("Child Level Id: " + String.valueOf(levelItem.getPostLevelId()));
            this.logger.info("Child Level Id: " + String.valueOf(levelItem.getPostLevelId()));
            childLevelId.add(levelItem.getPostLevelId());
            Collection<PostLevel> childs = this.getChildLevel(levelItem.getPostLevelId());
            //Find all child level ids recursively
            if (childs.size() != 0)
            {
                childLevelId.addAll(findChildLevelIds(getChildLevel(levelItem.getPostLevelId())));
            }
        }
        return childLevelId;
    }

}
