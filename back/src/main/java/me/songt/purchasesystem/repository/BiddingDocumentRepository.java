package me.songt.purchasesystem.repository;

import me.songt.purchasesystem.po.BiddingDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by tony on 2017/3/5.
 */
@Transactional
@Repository
public interface BiddingDocumentRepository extends PagingAndSortingRepository<BiddingDocument, Long>
{
    Page<BiddingDocument> findAll(Pageable pageable);

    BiddingDocument findBybiddingId(int id);

    BiddingDocument findBybiddingNum(String biddingNum);
}
