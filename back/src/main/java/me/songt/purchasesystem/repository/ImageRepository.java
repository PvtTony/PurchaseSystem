package me.songt.purchasesystem.repository;

import me.songt.purchasesystem.po.Image;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by tony on 2017/2/28.
 */
public interface ImageRepository extends CrudRepository<Image, Long>
{
    Image findByimageId(int id);

    Image findBysupplierId(int id);
}
