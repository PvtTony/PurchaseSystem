package me.songt.purchasesystem.repository;

import me.songt.purchasesystem.po.PostLevel;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by tony on 2017/2/23.
 * Post Level Dao
 */
@Transactional
public interface PostLevelRepository extends CrudRepository<PostLevel, Long>
{
    //Iterable<PostLevel> findByHasParentId(int parentId);
    Iterable<PostLevel> findBypostHasparent(int parentId);

    Iterable<PostLevel> findBypostPathStartsWith(String startPath);

    Iterable<PostLevel> findBypostDepth(int depth);

}
