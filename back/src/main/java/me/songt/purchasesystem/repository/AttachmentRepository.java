package me.songt.purchasesystem.repository;

import me.songt.purchasesystem.po.Attachment;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by tony on 2017/2/25.
 * Attachment Management Repository
 */
public interface AttachmentRepository extends CrudRepository<Attachment, Long>
{
    Attachment findByattachmentId(int id);

    Attachment findByattachmentBiddingId(int id);

}
