package me.songt.purchasesystem.repository;

import me.songt.purchasesystem.po.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by tony on 2016/12/10.
 */
@Transactional
@Repository
public interface PostRepository extends PagingAndSortingRepository<Post, Long>
{
    /*@Query("SELECT Post FROM Post WHERE Post.level IN :ids")
    Collection<Post> findBylevels(@Param("ids") Set<Integer> ids);*/

//    Page<Post> findByPostDeletedEqualsAndPostLevelIdInelIdIn(List<Integer> ids, Pageable pageable);

    Page<Post> findByPostDeletedEqualsAndPostLevelIdIn(int deleted, List<Integer> ids, Pageable pageable);

    Page<Post> findBypostDeletedEquals(int deleted, Pageable pageable);

    Post findBypostId(int id);


    //Post findOne(long postId, PageRequest request);
}
