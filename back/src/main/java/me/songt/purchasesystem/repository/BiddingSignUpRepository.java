package me.songt.purchasesystem.repository;

import me.songt.purchasesystem.po.BiddingSignUp;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by AUG01 on 2017/3/16.
 */
@Transactional
@Repository
public interface BiddingSignUpRepository extends PagingAndSortingRepository<BiddingSignUp, Long>
{
    List<BiddingSignUp> findBybiddingId(int biddingId);

    Long countBybiddingId(int biddingId);

}
