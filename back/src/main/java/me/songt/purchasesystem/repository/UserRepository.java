package me.songt.purchasesystem.repository;

import me.songt.purchasesystem.po.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by tony on 2017/3/4.
 * User Repository
 */
@Transactional
@Repository
public interface UserRepository extends CrudRepository<User, Long>
{
    User findByuserName(String name);

    User findByuserId(int userId);
//    Iterable<User> findByuserToken(String token);
}
