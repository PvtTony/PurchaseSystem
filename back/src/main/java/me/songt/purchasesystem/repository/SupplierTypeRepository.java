package me.songt.purchasesystem.repository;

import me.songt.purchasesystem.po.SupplierType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by AUG01 on 2017/3/17.
 */
@Transactional
@Repository
public interface SupplierTypeRepository extends PagingAndSortingRepository<SupplierType, Long>
{
    SupplierType findBysupplierTypeId(int supplierTypeId);
}
