package me.songt.purchasesystem.repository;

import me.songt.purchasesystem.po.Supplier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by AUG01 on 2017/3/16.
 */
@Transactional
@Repository
public interface SupplierRepository extends PagingAndSortingRepository<Supplier, Long>
{
    Page<Supplier> findAll(Pageable pageable);

    Supplier findBysupplierId(int supplierId);

//    List<Supplier> findBysupplierId(List<Integer> supplierId);
}
