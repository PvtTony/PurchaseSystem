package me.songt.purchasesystem.vo;

import java.util.List;

/**
 * Created by tony on 2017/2/23.
 * Post Level Tree Class
 */
public class PostLevelTree
{
    private List<PostLevelTree> childTree;
    private int levelId;
    private String levelName;
    private int levelDepth;


    public PostLevelTree(int levelId, String levelName, int levelDepth)
    {
        this.levelId = levelId;
        this.levelName = levelName;
        this.levelDepth = levelDepth;
    }

    public PostLevelTree()
    {
    }

    public List<PostLevelTree> getChildTree()
    {
        return childTree;
    }

    public void setChildTree(List<PostLevelTree> childTree)
    {
        this.childTree = childTree;
    }


    public int getLevelId()
    {
        return levelId;
    }

    public void setLevelId(int levelId)
    {
        this.levelId = levelId;
    }

    public String getLevelName()
    {
        return levelName;
    }

    public void setLevelName(String levelName)
    {
        this.levelName = levelName;
    }

    public int getLevelDepth()
    {
        return levelDepth;
    }

    public void setLevelDepth(int levelDepth)
    {
        this.levelDepth = levelDepth;
    }
}
