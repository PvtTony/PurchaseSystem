package me.songt.purchasesystem.vo;

/**
 * Created by tony on 2017/3/6.
 */
public class TokenDetail
{
    int userId;
    private String userName, token;

    public TokenDetail()
    {
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }
}
