package me.songt.purchasesystem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by tony on 2016/12/7.
 * Main Spring Boot Application Bean
 */
@SpringBootApplication
public class Application
{
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static Logger getLogger()
    {
        return logger;
    }

    public static void main(String[] args)
    {
        SpringApplication.run(Application.class);
        logger.info("Application Started");
    }

}
